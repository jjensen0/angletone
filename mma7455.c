/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "capi324v221.h"

signed char data = -1;
signed char data2 = -2;
signed char x = 0;
signed char y = 0;
signed char z = 0;

signed char gyro[8];
signed int GyroX, GyroY, GyroZ, GyroTemp;

void action(void);
void RegisterWrite(unsigned char, unsigned char);
unsigned char RegisterRead(unsigned char);
void MMA7455_init(void);

void CBOT_main( void )
{


	LCD_open();
	LCD_clear();

	//SPKR_open( SPKR_TONE_MODE );
	//STEPPER_open();

	// Open the I2C subsystem.
	I2C_open();
	//I2C_set_BRG( 10, I2C_PRESCALER_1 );
	// Enable the pullups.
	I2C_pullup_enable();

	//turn on mesurement mode
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x16 );
		I2C_MSTR_send( 0x05 );
		I2C_MSTR_stop();
	}

	//offset x
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x10 );
		I2C_MSTR_send( 0x15 );
		I2C_MSTR_stop();
	}
	//offset y
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x12 );
		I2C_MSTR_send( 0x34 );
		I2C_MSTR_stop();
	}
	//offset z
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x14 );
		I2C_MSTR_send( 0xEE );
		I2C_MSTR_stop();
	}
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x15 );
		I2C_MSTR_send( 0x0F );
		I2C_MSTR_stop();
	}
/*
while(1)
{
	for(int i = 0; i<127; i++)
	{
		if( I2C_MSTR_start( i, I2C_MODE_MT ) == I2C_STAT_OK )
		{
			LCD_printf_RC( 3, 0, "%d", i);
			break;
			I2C_MSTR_send( 0x00 );
		}
	}
}
	if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		LCD_printf_RC( 3, 0, "yolo");
		I2C_MSTR_send( 0x00 );
	}
	if ( I2C_MSTR_start( 0x68, I2C_MODE_MR ) == I2C_STAT_OK )
	{

		I2C_MSTR_get( &data, FALSE );

		I2C_MSTR_stop();
	} // end if()

	LCD_printf_RC( 0, 0, "%3d", data);

	if( I2C_MSTR_start( 0x69, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		LCD_printf_RC( 2, 0, "yolo2");
		I2C_MSTR_send( 0x00 );
	}
	if ( I2C_MSTR_start( 0x69, I2C_MODE_MR ) == I2C_STAT_OK )
	{

		I2C_MSTR_get( &data2, FALSE );

		I2C_MSTR_stop();
	} // end if()

	LCD_printf_RC( 0, 4, "%3d", data2);

}
*/
/*
	//power management default gyro
	if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x3E );
		I2C_MSTR_send( 0x00 );
		I2C_MSTR_stop();
	}

	//sample rate divider gyro
	//Fsample = 1kHz / (7 + 1) = 125Hz, or 8ms per sample
	if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x15 );
		I2C_MSTR_send( 0x07 );
		I2C_MSTR_stop();
	}

	//// +/- 2000 dgrs/sec, 1KHz, 1E, 19 gyro
	if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x16 );
		I2C_MSTR_send( 0x1E );
		I2C_MSTR_stop();
	}
	//interupts off gyro
	if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x17 );
		I2C_MSTR_send( 0x00 );
		I2C_MSTR_stop();
	}

*/
	while(1)
	{
		if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
		{
			I2C_MSTR_send( 0x06 );
		}

		if ( I2C_MSTR_start( 0x1D, I2C_MODE_MR ) == I2C_STAT_OK )
		{
			I2C_MSTR_get( &x, TRUE );

			I2C_MSTR_get( &y, TRUE );

			I2C_MSTR_get( &z, FALSE );

			I2C_MSTR_stop();
		} // end if()
/*
		if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
		{
			I2C_MSTR_send( 0x1B );
		}
		if ( I2C_MSTR_start( 0x68, I2C_MODE_MR ) == I2C_STAT_OK )
		{
			for(int i = 0; i<7; i++)
			{
				I2C_MSTR_get( &gyro[i], TRUE );
			}

			I2C_MSTR_get( &gyro[7], FALSE );

			I2C_MSTR_stop();
		} // end if()
*/
		LCD_printf_RC( 3, 0, "x: %3d", x);
		LCD_printf_RC( 2, 0, "y: %3d", y);
		LCD_printf_RC( 1, 0, "z: %3d", z);

		//GyroX = ((gyro[4] << 8) | gyro[5]);
		//GyroY = ((gyro[2] << 8) | gyro[3]);
		//GyroZ = ((gyro[6] << 8) | gyro[7]);
		//GyroTemp = (gyro[0] << 8) | gyro[1]; // temperature

/*		LCD_printf_RC( 3, 10, "Gx: %3d", GyroX);
		LCD_printf_RC( 2, 10, "Gy: %3d", GyroY);
		LCD_printf_RC( 1, 10, "Gz: %3d", GyroZ);
		LCD_printf_RC( 0, 10, "Gt: %3d", GyroTemp);
*/
		TMRSRVC_delay( 250 );
	}

}


unsigned char RegisterRead(unsigned char reg)
{
	unsigned char data;

	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( reg );
	}

	if ( I2C_MSTR_start( 0x1D, I2C_MODE_MR ) == I2C_STAT_OK )
	{
		I2C_MSTR_get( &data, FALSE );
		I2C_MSTR_stop();
	}

	return data;

}
void RegisterWrite(unsigned char reg, unsigned char data )
{
	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( reg );
		I2C_MSTR_send( data );
		I2C_MSTR_stop();
	}
}

void action(void)
{

	if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
	{
		I2C_MSTR_send( 0x06 );
	}

	if ( I2C_MSTR_start( 0x1D, I2C_MODE_MR ) == I2C_STAT_OK )
	{
		I2C_MSTR_get( &x, TRUE );

		I2C_MSTR_get( &y, TRUE );

		I2C_MSTR_get( &z, FALSE );

		I2C_MSTR_stop();
	} // end if()

	LCD_printf_RC( 3, 0, "x: %3d", x);
	LCD_printf_RC( 2, 0, "y: %3d", y);
	LCD_printf_RC( 1, 0, "z: %3d", z);

	if(x <= 0 && x >= -8)
	{
		SPKR_play_note( SPKR_NOTE_A, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "A");
	}
	else if(x <= -9 && x >= -16)
	{
		SPKR_play_note( SPKR_NOTE_B, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "B");
	}
	else if(x <= -17 && x >= -24)
	{
		SPKR_play_note( SPKR_NOTE_C, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "C");
	}
	else if(x <= -25 && x >= -32)
	{
		SPKR_play_note( SPKR_NOTE_D, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "D");
	}
	else if(x <= -33 && x >= -40)
	{
		SPKR_play_note( SPKR_NOTE_E, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "E");
	}
	else if(x <= -41 && x >= -48)
	{
		SPKR_play_note( SPKR_NOTE_F, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "F");
	}
	else if(x <= -49 && x >= -77)
	{
		SPKR_play_note( SPKR_NOTE_G, SPKR_OCTV3, 0, 250, 80 );
		LCD_printf_RC( 0, 0, "G");
	}

	TMRSRVC_delay( 250 );

}
void MMA7455_init(void)
{
		//turn on mesurement mode
			if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
			{
				I2C_MSTR_send( 0x16 );
				I2C_MSTR_send( 0x05 );
				I2C_MSTR_stop();
			}

			//clear offset x
			if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
			{
				I2C_MSTR_send( 0x10 );
				I2C_MSTR_send( 0x00 );
				I2C_MSTR_stop();
			}
			//clear offset y
			if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
			{
				I2C_MSTR_send( 0x12 );
				I2C_MSTR_send( 0x00 );
				I2C_MSTR_stop();
			}
			//clear offset z
			if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
			{
				I2C_MSTR_send( 0x14 );
				I2C_MSTR_send( 0x00 );
				I2C_MSTR_stop();
			}
			if( I2C_MSTR_start( 0x1D, I2C_MODE_MT ) == I2C_STAT_OK )
			{
				I2C_MSTR_send( 0x15 );
				I2C_MSTR_send( 0x00 );
				I2C_MSTR_stop();
			}
}

//while(1)
//{
	 //if ( ATTINY_get_SW_state( ATTINY_SW4 ) )
	 //{
		//action();
	 //} // end if()
//}


//STEPPER_move_rn( STEPPER_BOTH,
//STEPPER_FWD, 200, 200, // Left
//STEPPER_FWD, 200, 200 ); // Right
/*
		if(z < 50 || z > 78)
		{
			STEPPER_move_stwt( STEPPER_BOTH,
				 STEPPER_REV, 150, 250, 200, STEPPER_BRK_OFF,
				 STEPPER_FWD, 150, 250, 200, STEPPER_BRK_OFF );
		}

		STEPPER_move_rn( STEPPER_BOTH,
		STEPPER_FWD, 200, 200, // Left
		STEPPER_FWD, 200, 200 ); // Right
		*/
		//LCD_clear();

